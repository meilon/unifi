#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#
#  _   _       _  __ _ 
# | | | |     (_)/ _(_)
# | | | |_ __  _| |_ _ 
# | | | | '_ \| |  _| |
# | |_| | | | | | | | |
#  \___/|_| |_|_|_| |_|
#
# created: 01/2022
#
# Author: Frank Baier
# E-Mail: dev@baier-nt.de
#
from cmk.gui.plugins.metrics import translation

check_metrics["check_mk-unifi_device_if"] = translation.if_translation

#from cmk.gui.plugins.metrics import *
#check_metrics["check_mk-unifi_device_if"] = if_translation
